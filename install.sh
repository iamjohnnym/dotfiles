#!/usr/bin/env bash

DIRS=( brew pyenv rbenv vscode )
for DIR in "${DIRS[@]}"
do
    cd "${DIR}" || exit
    bash install.sh
    cd - || exit
done
