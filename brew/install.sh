#!/usr/bin/env bash

if ! command -v brew &> /dev/null
then
    echo "Installing Homebrew..."
    curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh
    echo "Done."
fi

brew bundle
