#!/usr/bin/env bash

INSTALLED_VERSIONS=$(pyenv versions) 

# shellcheck disable=SC2013
for PYVERSION in $(cat python-versions)
do
    if [[ ! $INSTALLED_VERSIONS =~ $PYVERSION  ]]
    then
        pyenv install "${PYVERSION}"
    fi
done 
