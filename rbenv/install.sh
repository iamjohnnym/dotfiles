#!/usr/bin/env bash

INSTALLED_VERSIONS=$(rbenv versions) 

# shellcheck disable=SC2013
for RUBYVERSION in $(cat ruby-versions)
do
    if [[ ! $INSTALLED_VERSIONS =~ $RUBYVERSION  ]]
    then
        rbenv install "${RUBYVERSION}"
    fi
done 
