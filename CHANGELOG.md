### [1.0.2](https://gitlab.com/iamjohnnymgg/dotfiles/compare/1.0.1...1.0.2) (2021-09-22)


### Bug Fixes

* add changelog ([e5c4cd3](https://gitlab.com/iamjohnnymgg/dotfiles/commit/e5c4cd358380b51a9173b0da2b6e2df760d48309))
* add this branch to test release ([c6ee0f8](https://gitlab.com/iamjohnnymgg/dotfiles/commit/c6ee0f881a43f5c324c2ccb79209dd9f95d130b4))
* assets included for gitlab?  no idea. ([08e29d9](https://gitlab.com/iamjohnnymgg/dotfiles/commit/08e29d9f3bbb2cd211e3b28943d042a711915ab1))
* change filepath to ~/.npmrc ([cb3f561](https://gitlab.com/iamjohnnymgg/dotfiles/commit/cb3f561450c17ea68e5562b22fb55a27d5f91bcd))
* install npm conventional-changelog-conventionalcommits ([4e7e2b4](https://gitlab.com/iamjohnnymgg/dotfiles/commit/4e7e2b4b37c9dc5b5bed964ff63c3832fffc8bee))
* linting ([d1e180a](https://gitlab.com/iamjohnnymgg/dotfiles/commit/d1e180af79c8b5c090faf25f06a831cdceb2d80e))
* linting ([86e439a](https://gitlab.com/iamjohnnymgg/dotfiles/commit/86e439a295f144c4e712a14fc88795ac2a22cccf))
* mimick gitlab releaserc ([a22c258](https://gitlab.com/iamjohnnymgg/dotfiles/commit/a22c258cd05ebd3b4b808f667b228b100f12866e))
* not sure what  im doing at this point ([ff9cf9b](https://gitlab.com/iamjohnnymgg/dotfiles/commit/ff9cf9b3f012c4e11b8326503e17e914985199ff))
* release on all branches. all the things! ([117c963](https://gitlab.com/iamjohnnymgg/dotfiles/commit/117c963c1ac8ae9dcb0a7e6ca9c6495e21b38983))
* test pipeline release ([bffc51b](https://gitlab.com/iamjohnnymgg/dotfiles/commit/bffc51bf3b29fe441bfa8395d282e2f9aed3d430))

### [1.0.1](https://gitlab.com/iamjohnnymgg/dotfiles/compare/1.0.0...1.0.1) (2021-09-22)


### Bug Fixes

* **.gitlab-ci.yml:** try packaging npm package ([f5423f1](https://gitlab.com/iamjohnnymgg/dotfiles/commit/f5423f10332e243065d92040446567bc8cf6a428))

## [1.0.0](https://gitlab.com/iamjohnnymgg/dotfiles/compare/...1.0.0) (2021-09-22)


### Bug Fixes

* **brew/install.sh:** added period for release tst ([8d7b6e9](https://gitlab.com/iamjohnnymgg/dotfiles/commit/8d7b6e94ad0a34f2c5c5ae0a18b3ee48d45859be))
